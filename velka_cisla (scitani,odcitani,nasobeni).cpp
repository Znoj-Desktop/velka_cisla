/*
 * Soubor:  velka_cisla.cpp
 * Datum:   2011/12/08
 * Autor:   
 * Projekt do ZP
 */

//pripojeni knihovny jazyka c++ pro vstup/vystupni operace
#include <iostream>

using namespace std;


/** \brief cte a kontroluje parametry prikazoveho radku
 *
 * Pokud neni zadan parametr, nebo pokud je zadan parametr "-h", 
 * pak se vola funkce pro vypis napovedy a program uspesne skonci.
 * Poud jsou zadany 2 parametry, pak funkce kontroluje jestli je zadana platna operace.
 * Pokud je zadan jiny nez drive uvedeny pocet parametru (3 a vice),
 * pak se vytiskne prislusne erroroveho hlaseni
 *
 * @param argc udava pocet zadanych parametru se kterymi je program spusten
 * @param *argv[] je pole ukazatelu ukazujicich na pamet ve ktere je ulozen obsah parametru prikazoveho radku
 */
void kontrola_parametru(int argc, char *argv[], int *operace)
{
	//pokud neni zadan zadny parametr, nebo prave jeden "-h", pak se vytiskne napoveda
	if((argc == 1) || ((argc == 2) && (strcmp("-h", argv[1]) == 0)))
	{
		cout << "Napoveda:" << endl;
		cout << "Kdyz se zada -h nebo nic, tak se vytiskne napoveda" << endl;
		cout << "Kdyz se zadaji 2 parametry, tak je to cesta ke vstupnimu souboru ze" << endl;
		cout << "ktereho se budou nacitat cisla se kterymi bude program pracovat, " << endl;
		cout << "druhym parametrem je operace ktera se bude s temito cisly provadet." << endl;
		cout << "Kdyz se zada parametr ktery neni cesta k souboru, nebo kdyz se" << endl;
		cout << "zada jen jeden parametr, nebo cislo ktere nereprezentuje zadnou" << endl;
		cout << "operaci, pak se vytiskne chyba" <<endl;
		system("PAUSE");
		exit(EXIT_SUCCESS);
	}

	//pokud ma program 2 parametry, pak by to mela byt cesta ke vstupnimu souboru a pozadovana operace
	else if(argc == 3)
	{
		*operace = atoi(argv[2]);
		if((*operace > 3) || (*operace < 1))
		{
			cout << "Neplatna operace - zadej bud \"1\" pro scitani, \"2\" pro odcitani nebo \"3\" pro nasobeni" << endl;
		}
	}
  
	//pokud je zadan jiny pocet parametru, pak se jedna o chybu
	else
	{
		cout << "zadano moc parametru nebo jen jeden. Oboji je chyba" << endl;
		system("PAUSE");
		exit(EXIT_FAILURE);
	} 
} 

/** \brief 
 *
 *
 */
 
int scitani(int pole_prvni[], int pole_druhy[], int vysledek[], int pocet_prvni, int pocet_druhy, int pocet_vysledek)
{
	bool prenos = 0;
	int soucet = 0;
	//cisla se scitaji od jednotek pres desitky, pak stovky az k nejvyssim pozicim
	for(int i = 0; i <= pocet_vysledek; i++)
	{
		//pokud maji jeste obe cisla znaky ktere by do souctu mohla poskytnout, pak se podminka
		//kladne vyhodnoti
		if(((pocet_prvni-i) >= 0) && ((pocet_druhy-i) >= 0))
		{
			//do promenne soucet se ulozi soucet cisel na prave zpracovavane pozici a k nemu
			//se pricte pripadny prenos z nizsiho radu
			soucet = pole_prvni[pocet_prvni-i] + pole_druhy[pocet_druhy-i] + prenos;
			//prenos se znovu inicializuje na 0;
			prenos = 0;
			//pokud je soucet vetsi nez 9, pak je potreba jej zpracovat a prenest 1 do vyssiho radu
			//zbytek znovu ulozime do promenne soucet
			if(soucet > 9)
			{
				soucet = soucet%10;
				prenos = 1;
			}
			//do vysledku ulozime cislo v promenne soucet
			vysledek[pocet_vysledek-i] = soucet;
		}

		//pokud uz nam zbylo jen jedno cislo, pak k nemu pricteme pripadny prenos z nizsiho radu
		//a cislo ulozime na spravou pozici do pole vysledek. I zde muze vzniknout dalsi prenos
		else if((pocet_prvni-i) >= 0)
		{
			soucet = pole_prvni[pocet_prvni-i]+prenos;
			//prenos se znovu inicializuje na 0;
			prenos = 0;
			//pokud je soucet vetsi nez 9, pak je potreba jej zpracovat a prenest 1 do vyssiho radu
			//zbytek znovu ulozime do promenne soucet
			if(soucet > 9)
			{
				soucet = soucet%10;
				prenos = 1;
			}
			//do vysledku ulozime cislo v promenne soucet
			vysledek[pocet_vysledek-i] = soucet;
		}

		//pokud uz nam zbylo jen jedno cislo, pak k nemu pricteme pripadny prenos z nizsiho radu
		//a cislo ulozime na spravou pozici do pole vysledek. I zde muze vzniknout dalsi prenos
		else if((pocet_druhy-i) >= 0)
		{
			soucet = pole_druhy[pocet_druhy-i]+prenos;
			//prenos se znovu inicializuje na 0;
			prenos = 0;
			//pokud je soucet vetsi nez 9, pak je potreba jej zpracovat a prenest 1 do vyssiho radu
			//zbytek znovu ulozime do promenne soucet
			if(soucet > 9)
			{
				soucet = soucet%10;
				prenos = 1;
			}
			//do vysledku ulozime cislo v promenne soucet
			vysledek[pocet_vysledek-i] = soucet;
		}

		//pokud uz zadne cislo nelze zpracovat, pak jeste muze byt do vysledku ulozen prenos do 
		//vyssiho radu. Pokud ne, pak do pole vysledek jiz nic neukladame, abychom si zachovali
		//nainicializovane "-1" a tim padem mohli vypsat jen tolik mist vysledku kolik je treba
		else
		{
			if(prenos)
			{
				vysledek[pocet_vysledek-i] = 1;
			}
			prenos = 0;
		}
	}
	//rozdil cisel - z nichz prvni je vetsi nez druhy je kladny
	return 0;
}

/** \brief 
 *
 *
 */

int odcitani(int pole_prvni[], int pole_druhy[], int vysledek[], int pocet_prvni, int pocet_druhy, int pocet_vysledek)
{
	bool prenos = 0;
	int rozdil = 0;

	//pokud je druhy cislo vetsi nez prvni, pak bude nejjednodussi to udelat tak, ze cisla prohodime
	//a zmenime znamenko vysledku
	if((pocet_druhy > pocet_prvni) || ((pocet_druhy == pocet_prvni) && (pole_druhy[0] > pole_prvni[0]))) 
	{
		odcitani(pole_druhy, pole_prvni, vysledek, pocet_druhy, pocet_prvni, pocet_vysledek);
		return 1;
	}

	//cisla se odcitaji na pozici jednotek pres desitky, pak stovky az k nejvyssim pozicim
	for(int i = 1; i < pocet_vysledek; i++)
	{
		//pokud je co od ceho odcitat, pak se podminka vyhodnoti kladne
		if(((pocet_prvni-i) >= 0) && ((pocet_druhy-i) >= 0))
		{
			//do promenne rozdil se ulozi rozdil cisel na prave zpracovavane pozici a k nemu
			//se pricte pripadny prenos z nizsiho radu
			rozdil = pole_prvni[pocet_prvni-i] - pole_druhy[pocet_druhy-i] - prenos;
			//prenos se znovu inicializuje na 0;
			prenos = 0;
			//pokud je rozdil mensi jak 0, pak je potreba poresit prenos
			if(rozdil < 0)
			{
				if((pocet_prvni-(i+1)) >= 0)
				{
					prenos = 1;
				}

				rozdil += 10;

			}
			//do vysledku ulozime cislo v promenne rozdil
			vysledek[pocet_vysledek-i] = rozdil;
		}

		//vysledek muzeme kvuli pocatecni podmince primo ulozit jako vysledek, protoze se nikdy
		//nedostaneme do situace ze by byl potreba nejakej prenos
		else if((pocet_prvni-i) >= 0)
		{
			vysledek[pocet_vysledek-i] = pole_prvni[pocet_prvni-i] - prenos;
			prenos = 0;
		}
	}


	return 0;
}

/** \brief 
 *
 *
 */

void nasobeni(int pole_prvni[], int pole_druhy[], int vysledek[], int pocet_prvni, int pocet_druhy, int pocet_vysledek)
{
	int prenos = 0;
	int nasobek = 0;

	//alokujeme jeste 2 pomocne pole do kterych budeme ukladat prubezne vysledky
	int *pomocne;
	pomocne = (int *) malloc (sizeof(int) * (pocet_vysledek));

	int *pomocne2;
	pomocne2 = (int *) malloc (sizeof(int) * (pocet_vysledek));
	
	//pole inicializujeme na 0
	for(int i = 0; i < pocet_vysledek; i++)
	{   
		pomocne[i] = 0;
		pomocne2[i] = 0;
	}

	//pokud v poli jeste neco je a pokud nezasahujeme mimo pole muzeme pocitat
	if(pole_druhy[pocet_druhy-1] > 0)
	{
		for(int j = 1; j <= pocet_prvni; j++)
		{
			//do promenne nasobek se vynasobi 2 aktualni cisla a k nim se pricte pripadny prenos z nizsiho radu
			nasobek = pole_druhy[pocet_druhy-1] * pole_prvni[pocet_prvni-j] + prenos;
			//prenos se pouzil, tak se vynuluje
			prenos = 0;
			//pokud je cislo 2ciferne, pak se prvni cifra ulozi jako prenos a druha se ulozi do vysledku
			if(nasobek > 9)
			{
				pomocne[pocet_vysledek-j] = nasobek % 10;
				prenos = nasobek / 10;
			}
			//prenos neni
			else
			{
				pomocne[pocet_vysledek-j] = nasobek;
			}
		}

		if(prenos > 0)
		{
			pomocne[pocet_vysledek-(pocet_prvni+1)] = prenos;
			prenos = 0;
		}

	}

	//cisla se nasobi od jednotek pres desitky, pak stovky az k nejvyssim pozicim
	//v kazdem cyklu vzdy prez vsechny cislice v prvnim cisle
	for(int i = 2; i <= pocet_druhy; i++)
	{
		//pokud v poli jeste neco je
		if(pole_druhy[pocet_druhy-i] > 0)
		{
			for(int j = 1; j <= pocet_prvni; j++)
			{
				//do promenne nasobek se vynasobi 2 aktualni cisla a k nim se pricte pripadny prenos z nizsiho radu
				nasobek = pole_druhy[pocet_druhy-i] * pole_prvni[pocet_prvni-j] + prenos;
				//prenos se pouzil, tak se vynuluje
				prenos = 0;
				//pokud je cislo 2ciferne, pak se prvni cifra ulozi jako prenos a druha se ulozi do vysledku
				if(nasobek > 9)
				{
					pomocne2[pocet_vysledek-(j+i-1)] = nasobek % 10;
					prenos = nasobek / 10;
				}
				//prenos neni
				else
				{
					pomocne2[pocet_vysledek-(j+i-1)] = nasobek;
				}

				//na nejnizsi pozice kde nejsou cisla je potreba dosadit nuly
				for(int k = 1; k < i; k++)
				{
					pomocne2[pocet_vysledek - k] = 0;
				}
			}

			if(prenos > 0)
			{
				pomocne2[pocet_vysledek-(pocet_prvni+i)] = prenos;
				prenos = 0;
			}
		}

		//scitame vzdy pole pro 2 spocitane cislice
		//navratova hodnota nas nezajima
		scitani(pomocne, pomocne2, vysledek, pocet_vysledek, pocet_vysledek, pocet_vysledek);
		
		//ulozeni vysledku do pomocneho pole pomocne
		for(int i = 0; i < pocet_vysledek; i++)
		{   
			if(vysledek[i] != -1)
			{
				pomocne[i] = vysledek[i];
			}
		}
	}
}

/** \brief vypis vysledku v jazyce html
 *
 * Zapise do souboru, jehoz odkaz je zadan prvnim parametrem se kterym je funkce volana, HTML kod, ve kterem budou
 * zadana cisla a vysledek.
 *
 * @param *soubor ukazatel na soubor do ktereho chceme HTML kod zapsat
 * @param ...
 */
void vypis (FILE *soubor, int operace, int pole_prvni[], int pole_druhy[], int pole_vysledek[], bool znamenko1, bool znamenko2, bool znamenko_vysledek, int pocet_prvni, int pocet_druhy, int pocet_vysledek)
{	
	fprintf(soubor, "<HTML>\r\n");
	fprintf(soubor, "<HEAD>\r\n");
	fprintf(soubor, "     <TITLE>ZP - operace s velkymi cisly</TITLE>\r\n");
	fprintf(soubor, "</HEAD>\r\n");
	fprintf(soubor, "<BODY BGCOLOR=\"#0BB00F\">\r\n");
	fprintf(soubor, "<CENTER>\r\n <H1> S��t�n�, od��t�n� nebo n�soben� velk�ch ��sel </H1>\r\n\r\n");
	fprintf(soubor, "<H3>");
	
	
	//vypis znamekna u prvniho cisla pokud je treba
	if(znamenko1 == 1)
	{
		fprintf(soubor, "-");
	}
	//vypis prvniho cisla
	for(int i = 0; i < pocet_prvni; i++)
	{
		fprintf(soubor, "%d", pole_prvni[i]);
	}

	//vypis znamenka provadene operace
	if(operace == 1)
	{
		fprintf(soubor, " + ");
	}
	if(operace == 2)
	{
		fprintf(soubor, " - ");
	}
	if(operace == 3)
	{
		fprintf(soubor, " * ");
	}

	//vypis znamenka u druheho cisla pokud je treba
	if(znamenko2 == 1)
	{
		fprintf(soubor, "-");
	}
	//vypis druheho cisla
	for(int i = 0; i < pocet_druhy; i++)
	{
		fprintf(soubor, "%d", pole_druhy[i]);
	}
	
	fprintf(soubor, " = </H3>\r\n<H2><u><b>");

	//vypis vysledku
	if(znamenko_vysledek == 1)
	{
		fprintf(soubor, " -");
	}
	for(int i = 0; i < (pocet_vysledek+1); i++)
	{
		//vypisuje se jen tolik mist kolik je treba
		if((i == 0) && (pole_vysledek[i] == 0))
		{
			//pokud jsou na zacatku nejaky nuly - to se stane kdyz se odcitaji treba 2 3ciferny
			//cisla a vysledek je 2 - nebo 1ciferny cislo, tak jsou pak na zacatku nuly a ty nevypisujeme
			while(pole_vysledek[i] == 0)
			{
				i++;
			}
		}

		if(pole_vysledek[i] > -1)
		{
			fprintf(soubor, "%d", pole_vysledek[i]);
		}
	}

	fprintf(soubor, " </b></u></H2><br>\r\n\r\n");
	fprintf(soubor, "<H5>tato str�nka je vygenerov�na programem <i>velka_cisla.cpp</i> <br>\r\n");
	fprintf(soubor, "</CENTER> \r\n");
	fprintf(soubor, "</BODY>\r\n");
	fprintf(soubor, "<HTML>\r\n");
} 


int main(int argc,char** argv)
{
	int operace = 0;
	bool znamenko1 = 0;
	bool znamenko2 = 0;
	bool znamenko_vysledek = 0;
	
	kontrola_parametru(argc, argv, &operace);
	
	FILE *vstup;
	//otevreni souboru
	vstup = fopen(argv[1], "r");

	//kontrola, jestli se podarilo soubor spravne otevrit
	if (vstup == NULL)
	{
		cout << "chyba pri otevirani souboru" << endl;
		system("Pause");
		exit(EXIT_FAILURE);
	}
  
	unsigned long int pocet_prvni = 0;
	unsigned long int pocet_druhy = 0;

	int znak;
	//projizdim vstup, ale nemuzu ho ukladat protoze nevim jak velke pole naalokovat,
	//proto budu cist ze souboru 2x. Nejprve spocitam pocet cisel, pote tato cisla ulozim do pole
	//nacteni jednoho znaku do promenne znak
	znak = fgetc(vstup);
	
	//pokud je zde znamenko minus, pak si to zaznamename a nacteme dalsi znak
	if(znak == 45)
	{
		znamenko1 = 1;
		znak = fgetc(vstup);
	}
	//pocita znaky ktery jsou cisla az do prvni mezery
	do
	{
		//znak se pocita jen pokud je to cislo
		if(isdigit(znak))
		{
			pocet_prvni++;
		}

		//pokud znak cislo neni, pak je to chyba a je potreba zavrit soubor a skoncit
		if(!isdigit(znak))
		{
			cout << "chyba, chtel jsem jen cisla - \"cislo mezera cislo\" "<< endl;
			fclose(vstup);
			system("PAUSE");
			exit(EXIT_FAILURE);

		}

		//nacteni jednoho znaku do promenne znak
		znak = fgetc(vstup);
	}while(znak != ' ');

	//nacteni jednoho znaku do promenne znak - protoze jinak tam zustala mezera a ta
	//neni cislo...
	znak = fgetc(vstup);
	//pokud je zde znamenko minus, pak si to zaznamename a nacteme dalsi znak
	if(znak == 45)
	{
		znamenko2 = 1;
		znak = fgetc(vstup);
	}
	do
	{
		//znak se pocita jen pokud je to cislo
		if(isdigit(znak))
		{
			pocet_druhy++;
		}

		//pokud znak neni cislo, pak je to chyba a je potreba soubor zavrit a skoncit
		if(!isdigit(znak))
		{
			cout << "chyba, chtel jsem jen cisla - zapis do souboru \"cislo mezera cislo\"" << endl;
			fclose(vstup);
			system("PAUSE");
			exit(EXIT_FAILURE);
		}

		//nacteni jednoho znaku do promenne znak
		znak = fgetc(vstup);
	}while(znak != EOF);


	//vysledek bude mit maximalne o jeden znak vice nez vetsi cislo ze 2 cisel
	//ulozenych v souboru. Proto zjistim ktere cislo to je
	int pocet_vetsi = pocet_prvni;
	if(pocet_druhy > pocet_prvni)
	{
		pocet_vetsi = pocet_druhy;
	}  

	//pokud je zadana operace nasobeni, pak vysledek bude mit maximalne tolik cislic
	//kolik maji 2 cisla na vstupu v souctu (cislic)
	if(operace == 3)
	{
		pocet_vetsi = pocet_prvni + pocet_druhy;
	}

	//dynamicka alokace poli
	//do pole "pole_prvni" se nejdrive nacte cislo ze vstupniho souboru,
	int *pole_prvni;
	pole_prvni = (int *) malloc (sizeof(int) * (pocet_prvni));
  
	//do pole "pole_druhy" se nacte druhy cislo ze vstupniho souboru
	int *pole_druhy;
	pole_druhy = (int *) malloc (sizeof(int) * (pocet_druhy));
	
	//alokujeme pamet o 1 vetsi nez vetsi cislo, aby se tam vysledek vlezl
	int *vysledek;
	vysledek = (int *) malloc (sizeof(int) * (pocet_vetsi+1));

	//vsechny pole inicializovany na -1 - tato hodnota slouzi i jako zarazka
	for(int i = 0; i < pocet_prvni; i++)
	{                 
		pole_prvni[i] = -1;
	}
  
	for(int i = 0; i < pocet_druhy; i++)
	{                 
		pole_druhy[i] = -1;
	}

	for(int i = 0; i < (pocet_vetsi+1); i++)
	{                 
		vysledek[i] = -1;
	}

	//funkce zajisti, ze muzeme znovu cist soubor po znacich od zacatku
	//zde tedy cteme soubor podruhe a ukladame do pole
	rewind(vstup);
	//prepis cisel do poli
	znak = fgetc(vstup);
	//pokud je v cisle minus, pak jej preskocime - nacitame jen cisla
	if(znamenko1 == 1)
	{
		znak = fgetc(vstup);
	}

	for(int i = 0; i < pocet_prvni; i++)
	{
		//cislo 48 odecitam proto, protoze je v promenne znak ulozeno cislo tak jak je v
		//ASCII tabulce, tak abychom s nim mohli pracovat jako s cislem tak je potreba jej
		//takto upravit
		pole_prvni[i] = znak-48;
		znak = fgetc(vstup);
	}
	
	//preskoceni mezery
	znak = fgetc(vstup);
	//pokud je v cisle minus, pak jej preskocime - nacitame jen cisla
	if(znamenko2 == 1)
	{
		znak = fgetc(vstup);
	}

	for(int i = 0; i < pocet_druhy; i++)
	{
		//cislo 48 odecitam proto, protoze je v promenne znak ulozeno cislo tak jak je v
		//ASCII tabulce, tak abychom s nim mohli pracovat jako s cislem tak je potreba jej
		//takto upravit
		pole_druhy[i] = znak-48;
		znak = fgetc(vstup);
	}


	//vzhledem k tomu ze pracujeme s kladnymi i zapornymi cisly, tak je zde nekolik 
	//situaci ktere mohou nastat.
	if(operace == 1)
	{
		//scitani 2 kladnych cisel
		if((znamenko1 == 0) && (znamenko2 == 0))
		{
			znamenko_vysledek = scitani(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, (pocet_vetsi+1));
		}
    
		//scitaji se 2 zaporna cisla
		if((znamenko1 == 1) && (znamenko2 == 1))
		{
			znamenko_vysledek = scitani(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, (pocet_vetsi+1));
			//pri scitani 2 zapornych cisel je vysledek zaporny
			znamenko_vysledek = 1;
		}
    
		//prvni cislo zaporny a druhy kladny
		if((znamenko1 == 1) && (znamenko2 == 0))
		{
			znamenko_vysledek = odcitani(pole_druhy, pole_prvni, vysledek, pocet_druhy, pocet_druhy, (pocet_vetsi+1));
		}
    
		//prvni cislo kladny, druhy zaporny
		if((znamenko1 == 0) && (znamenko2 == 1))
		{
			znamenko_vysledek = odcitani(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, (pocet_vetsi+1));
		}
	}
  
	if(operace == 2)
	{
		//odcitani 2 kladnych cisel
		if((znamenko1 == 0) && (znamenko2 == 0))
		{
			znamenko_vysledek = odcitani(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, (pocet_vetsi+1));
		}
    
		//odcitani 2 zapornych cisel
		if((znamenko1 == 1) && (znamenko2 == 1))
		{
			znamenko_vysledek = odcitani(pole_druhy, pole_prvni, vysledek, pocet_druhy, pocet_prvni, (pocet_vetsi+1));
		}
    
		//prvni kladne druhe zaporne
		if((znamenko1 == 0) && (znamenko2 == 1))
		{
			znamenko_vysledek = scitani(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, (pocet_vetsi+1));
		}

		//prvni zaporne druhe kladne
		if((znamenko1 == 1) && (znamenko2 == 0))
		{
			znamenko_vysledek = scitani(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, (pocet_vetsi+1));
			//pri scitani 2 zapornych cisel je vysledek zaporny
			znamenko_vysledek = 1;
		}
	}

	if(operace == 3)
	{
		//nasobeni 2 kladnych, nebo 2 zapornych cisel
		if(((znamenko1 == 0) && (znamenko2 == 0)) || ((znamenko1 == 1) && (znamenko2 == 1)))
		{
			znamenko_vysledek = 0;
			nasobeni(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, pocet_vetsi);
		}

		//nasobeni 2 ruznych (co se znamenek tyce) cisel
		if(((znamenko1 == 1) && (znamenko2 == 0)) || ((znamenko1 == 0) && (znamenko2 == 1)))
		{
			znamenko_vysledek = 1;
			nasobeni(pole_prvni, pole_druhy, vysledek, pocet_prvni, pocet_druhy, pocet_vetsi);
		}
	}
  
	FILE *soubor;
	//otevreni souboru
	soubor = fopen("velka_cisla.html", "w");

	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor == NULL)
	{
		cout << "chyba pri otvirani souboru" << endl;
		free(pole_prvni);
		free(pole_druhy);
		free(vysledek);
		fclose(vstup);
		exit(EXIT_FAILURE);
	}

	//vypis do souboru HTML
	vypis(soubor, operace, pole_prvni, pole_druhy, vysledek, znamenko1, znamenko2, znamenko_vysledek, pocet_prvni, pocet_druhy, pocet_vetsi);
	
	//vypis na standardni vystup (stdout)
	//-----------------------------------------------------------------------------------------------------------------
	//vypis znamekna u prvniho cisla pokud je treba
	if(znamenko1 == 1)
	{
		cout << "-";
	}
	//vypis prvniho cisla
	for(int i = 0; i < pocet_prvni; i++)
	{
		cout << pole_prvni[i];
	}

	//vypis znamenka provadene operace
	if(operace == 1)
	{
		cout << " + ";
	}
	if(operace == 2)
	{
		cout << " - ";
	}
	if(operace == 3)
	{
		cout << " * ";
	}

	//vypis znamenka u druheho cisla pokud je treba
	if(znamenko2 == 1)
	{
		cout << "-";
	}
	//vypis druheho cisla
	for(int i = 0; i < pocet_druhy; i++)
	{
		cout << pole_druhy[i];
	}
	
	cout << " = ";

	//vypis vysledku
	if(znamenko_vysledek == 1)
	{
		cout << "-";
	}
	for(int i = 0; i < (pocet_vetsi+1); i++)
	{
		//vypisuje se jen tolik mist kolik je treba
		if((i == 0) && (vysledek[i] == 0))
		{
			//pokud jsou na zacatku nejaky nuly - to se stane kdyz se odcitaji treba 2 3ciferny
			//cisla a vysledek je 2 - nebo 1ciferny cislo, tak jsou pak na zacatku nuly a ty nevypisujeme
			while(vysledek[i] == 0)
			{
				i++;
			}
		}

		if(vysledek[i] > -1)
		{
			cout << vysledek[i];
		}
	}

	cout << endl;
	//------------------------------------------------------------------------------------------------------------------

	//uzavreni souboru
	fclose(soubor);
	fclose(vstup);
	//uvoln�n� poli 
	free(pole_prvni);
	free(pole_druhy);
	//realokovani pameti je proto, ze to v urcitych pripadech vyhazovalo chybu pri dealokaci
	//pole vysledek
	int * pomoc;
	pomoc = (int*) realloc (vysledek, 1*sizeof(int));
	free(pomoc);
	system("PAUSE");
	return 0;
}