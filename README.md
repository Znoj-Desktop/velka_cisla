### **Description**
Jedná se o program zpracovávající jednoduché matematické operace nad čísly. Čísla mohou mít velké množství cifer (i nad 10 tisíc). Program bude pracovat s aritmetickými operacemi sčítání, odčítání a násobení.

Vstupy: Program dostane na svůj vstup dvě libovolně velká čísla oddělena znaménkem operace  

12345678901234567890 + 12345678901234567890  
12345678901234567890 - 12345678901234567890  
12345678901234567890 x 12345678901234567890  
12345678901234567890 / 12  
12345678901234567890 ^ 12  

Výstupy: Jako výstup program vygeneruje HTML stránku, která bude obsahovat zadání (vstup) a také výsledek operace.  

12345678901234567890  
12345678901234567890  
--------------------
24691357802469135780  

Pro operaci násobení bude potřeba, aby ve výsledku byl zobrazený i posun při násobení druhou, třetí … cifrou.  

---
### **Technology**
C++

---
### **Year**
2011

---
### **Screenshot**
![](./README/velka_cisla.png)